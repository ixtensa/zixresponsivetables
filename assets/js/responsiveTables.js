/**
 * responsiveTable.js
 * Author & copyright (c) 2015: Hakan Havutcuoglu
 * GNU LGPL 3+
 *
 * Page: http://www.havutcuoglu.com
 *
 * jQuery plugin for responsiveTables on smartphones or small screens
 *
 */

// jQuery
(function($) {
	$(document).ready(function() {
		$('.responsiveTable').each(function(){
			// set the headline elements and rows for use
			var headertext = [],

			headers = this.querySelectorAll('th'),
			tablerows = this.querySelectorAll('tr'),
			tablebody = this.querySelector('tbody');

			// check table has thead or not, if not take the td elements from first tr
			if(headers.length == 0) {
				headers = this.querySelectorAll('tr:first-child td');
				// hide first tr 
				$(this).find('tr:first-child').css({
					'display': 'none'
				});
			}
			console.log(headers.length);
	
			// take and replace elements
			for(var i = 0; i < headers.length; i++) {
				var current = headers[i];
				headertext.push(current.textContent.replace(/\r?\n|\r/,""));
			}
			for (var i = 0, row; row = tablebody.rows[i]; i++) {
				for (var j = 0, col; col = row.cells[j]; j++) {
					col.setAttribute('data-th', headertext[j]);
				} 
			}
		});
	});
})(jQuery);