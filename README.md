
# README #

!!! IMPORTANT NOTICE !!!

* Version: Extension include scripts only for Contao >3.2, <=3.5

### What is this repository for? ###

* The Responsive Tables jQuery plugin for stacking tables on small screens. The purpose of responsiveTable.js is to give you an easy way of converting wide tables to a format that will work better on small screens.
* Version: 1.0.0

### How do I get set up? ###

1. Copy folder to */system/modules/*.
2. Add the class *responsiveTable* on your table or on division arround the table, script check only the main column from Contao.
	for example:

		<div class="responsiveTable">
			<!-- or use on table tag the class responsiveTables but not on both of them -->
			<table>
				<tr>
					<td>Titel 1</td>
					<td>Titel 2</td>
					<td>Titel 3</td>
				</tr>
				<tr>
					<td>foo</td>
					<td>goo</td>
					<td>hoo</td>
				</tr>
				<tr>
					<td>foo</td>
					<td>goo</td>
					<td>hoo</td>
				</tr>
			</table>
		</div>
	
3. If it exist a *<thead> <th>* this will be added on all *<td>* elements in *<tbody>*, if not, script will take the *<td>* element in first *<tr>* and use this as *<thead>*

### How it's work? ###

The extension check, in main column a table and the class *responsiveTables* exist.

If it's exist will take the *<thead> <th>* or first *<tr><td>* and add this on all *<td>* elements as attribute *'data-th'* (happen autoticaly).

The attribute *'data-th'* will added as :before selector on the same *<td>* element.

There is a css included for the standart representation. You find it in the *system/modules/zixResponsiveTables/assets/css/responsiveTables.scss* and you can custumize or overwrite it.


### Who do I talk to? ###

* Hakan Havutcuoglu
* info@havutcuoglu.com