<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2013 Leo Feyer
 *
 * @package   responsiveTables
 * @author    Hakan Havutcuoglu
 * @license   GNU
 * @copyright IXTENSA
 */


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	//Models
	//'responsiveTablesModel'  => 'system/modules/zixResponsiveTables/models/responsiveTablesModel.php',
));

/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	//'j_swipebox'  => 'system/modules/zixSwipebox/templates', // PHP-Variablen in JS-Variablen konvertieren

));