<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2013 Leo Feyer
 *
 * @package   responsiveTables
 * @author    Hakan Havutcuoglu
 * @license   GNU License
 * @copyright IXTENSA (c) 2015
 */

// Check it's mobile

$objEnvironment = Environment::getinstance();
$ua = $objEnvironment->agent;
 
//echo $ua->os;      // Operation System
//echo $ua->browser; // Browser name
//echo $ua->shorty;  // Browser shortcut
//echo $ua->version; // Browser version
//echo $ua->mobile;  // True if the Client a mobile browser
//echo $ua->class;   // CSS class-string

if ($ua->mobile) {
	
	if (TL_MODE == 'FE') {
		
		// JS
		$GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/zixResponsiveTables/assets/js/responsiveTables.js|static';
		
		// CSS
		$GLOBALS['TL_CSS'][] = 'system/modules/zixResponsiveTables/assets/css/responsiveTables.scss|static';

	}

}